package com.example.myapplication

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.TextView
import android.widget.Toast
import kotlin.math.sqrt

class MainActivity : AppCompatActivity() {

    private lateinit var resultTextView: TextView
    private var operand: Double = 0.0
    private var operation: String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        resultTextView = findViewById(R.id.textView2)

    }

    fun numberClick(clickedView: View) {

        if (clickedView is TextView) {

            var result = resultTextView.text.toString()
            val number = clickedView.text.toString()

            if (result == "0") {
                result = ""
            }

            resultTextView.text = result + number
        }

    }

    fun operationClick(clickedView: View) {

        if (clickedView is TextView) {

            val result = resultTextView.text.toString()

            if (result.isNotEmpty()) {
                operand = result.toDouble()

                operation = clickedView.text.toString()
                resultTextView.text = ""
            }
        }
    }

    fun equals(clickedView: View) {

        val seqOperandText = resultTextView.text.toString()
        var secOperand: Double = 0.0

        if (seqOperandText.isNotEmpty()) {
            secOperand = seqOperandText.toDouble()
        }
        when (operation) {

            "+" -> resultTextView.text = (operand + secOperand).toString()
            "-" -> resultTextView.text = (operand - secOperand).toString()
            "*" -> resultTextView.text = (operand * secOperand).toString()
            "/" -> {
                if (seqOperandText == "0") {
                    Toast.makeText(applicationContext, "Can't divide by zero.", Toast.LENGTH_SHORT).show()
                }else {
                    resultTextView.text = (operand / secOperand).toString() }
                }
            "%" -> resultTextView.text = ((operand * secOperand) / 100).toString()
            "√" -> resultTextView.text = ((sqrt(operand)).toString())
        }
    }

    fun clear(clickedView: View) {
        if (clickedView is TextView) {
            resultTextView.text = ""
        }
    }
}